FROM docker:26.0.0-rc1-dind-alpine3.19
RUN apk add --update --no-cache aws-cli openrc docker libgcc libstdc++ ncurses-libs icu-libs icu-data-full
RUN wget https://download.visualstudio.microsoft.com/download/pr/751363fb-4dd6-4bdf-a392-35210dd7c03e/f8759108ec5b0e724266b0b41a433f89/dotnet-sdk-8.0.201-linux-musl-x64.tar.gz
RUN mkdir -p /root/.dotnet && tar zxf dotnet-sdk-8.0.201-linux-musl-x64.tar.gz -C /root/.dotnet && rm dotnet-sdk-8.0.201-linux-musl-x64.tar.gz
ENV PATH="${PATH}:/root/.dotnet:/root/.dotnet/tools"
RUN rc-update add docker boot
